import { GridTableLayout } from './GridTableLayout';
import { Layout } from 'react-grid-layout';

const elements: Layout[] = [
  { i: "a", x: 4, y: 0, w: 1, h: 2 },
  { i: "b", x: 10, y: 2, w: 3, h: 2, minW: 2, maxW: 4 },
  { i: "c", x: 15, y: 1, w: 1, h: 1 }
];

const App = () => {
  return <GridTableLayout from={5} to={25} elements={elements}/>
};

export default App;

import { useRef, useState, useEffect, RefObject } from 'react';

interface Rect {
  x: number;
  y: number;
  width: number;
  height: number;
}

const useCanvas = (draw: (ctx: CanvasRenderingContext2D, rect: Rect) => void): RefObject<HTMLCanvasElement> => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [rect, setRect] = useState<Rect>({ x: 0, y: 0, width: window.innerWidth, height: window.innerHeight });
  const [isDragging, setIsDragging] = useState(false);
  const [dragOffset, setDragOffset] = useState<{ x: number; y: number } | null>(null);
  const [scale, setScale] = useState(1);

  useEffect(() => {
    const canvas = canvasRef.current!;
    const ctx = canvas.getContext('2d');
    if (!ctx) return;

    const handleResize = () => {
      if (canvas.clientWidth !== canvas.width || canvas.clientHeight !== canvas.height) {
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
        setRect((prevRect) => ({ ...prevRect, width: canvas.width, height: canvas.height }));
        draw(ctx, rect);
      }
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [canvasRef, rect, draw]);

  const handleMouseDown = (event: React.MouseEvent<HTMLCanvasElement>) => {
    setIsDragging(true);
    const { clientX, clientY } = event;
    const canvas = canvasRef.current;
    if (!canvas) return;
    const rect = canvas.getBoundingClientRect();
    setDragOffset({ x: clientX - rect.left, y: clientY - rect.top });
  };

  const handleMouseMove = (event: MouseEvent) => {
    if (!isDragging || !dragOffset) return;
    const { clientX, clientY } = event;
    const canvas = canvasRef.current;
    if (!canvas) return;
    const rect = canvas.getBoundingClientRect();
    const x = clientX - rect.left - dragOffset.x;
    const y = clientY - rect.top - dragOffset.y;
    setRect((prevRect) => ({ ...prevRect, x, y }));
  };

  const handleMouseUp = () => {
    setIsDragging(false);
    setDragOffset(null);
  };

  const handleWheel = (event: React.WheelEvent<HTMLCanvasElement>) => {
    event.preventDefault();
    const { deltaY } = event;
    const newScale = Math.max(0.1, scale - deltaY / 1000);
    const canvas = canvasRef.current;
    if (!canvas) return;
    const rect = canvas.getBoundingClientRect();
    const x = rect.left - (rect.left - rect.width / 2) * (newScale / scale);
    const y = rect.top - (rect.top - rect.height / 2) * (newScale / scale);
    setScale(newScale);
    setRect((prevRect) => ({ ...prevRect, x, y, width: rect.width * newScale, height: rect.height * newScale }));
  };

  useEffect(() => {
    const canvas = canvasRef.current!;
    const ctx = canvas.getContext('2d');
    if (!ctx) return;

    draw(ctx, rect);
  }, [canvasRef, draw, rect, scale]);

  return canvasRef;
};

export default useCanvas;
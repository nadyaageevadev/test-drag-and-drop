import React, { useState } from "react";
import GridLayout from "react-grid-layout";
import {Layout } from 'react-grid-layout';
import "./GridTableLayout.css";

type GridTableLayoutProps = {
    from: number;
    to: number;
    elements: Layout[];
};

export function GridTableLayout ({to, from, elements}: GridTableLayoutProps)  {
    const [layout, setLayout] = useState(elements);

    const [scaleLimits, setScaleLimits] = useState({from, to});
    const columnsCount = scaleLimits.to-scaleLimits.from;
 
    const handleWheel = (event: {preventDefault: () => void; deltaY: number;}) => {
        event.preventDefault();

        if (event.deltaY < 0) {
            setScaleLimits(prevScaleLimits => ({from: prevScaleLimits.from + 1, to: prevScaleLimits.to - 1}))
            setLayout(prevLayout => prevLayout.map(item => ({ ...item, x: item.x - 1 })));
        } else {
            setScaleLimits(prevScaleLimits => ({from: prevScaleLimits.from - 1, to: prevScaleLimits.to + 1}))
            setLayout(prevLayout => prevLayout.map(item => ({ ...item, x: Math.max(1, item.x + 1) })));
        }
        // тут остались мелкие баги в сценариях нахождения элемента у границы таблицы
      };

    const renderScale = React.useMemo(() => {
        const scaleItemsNumbers = Array.from({length: columnsCount }, (_, i) => scaleLimits.from + i);
        const colWidth = 1/columnsCount*100;

        return scaleItemsNumbers.map((value, index) => (
            <div key={index} style={{width: `${colWidth}%`}} className="scale-item">
                {value}
            </div>
        ))
    }, [scaleLimits]);

    React.useEffect(() => {
        window.addEventListener('wheel', handleWheel);

        return () => {
          window.removeEventListener('wheel', handleWheel);
        };
    }, [layout]);

    const onLayoutChange = (newAllLayouts: Layout[]) => {
        setLayout(newAllLayouts);
    };

    const layoutToRender = layout.filter(item => item.x >= 0);

    return (
        <div onWheel={handleWheel} className="root">
            <GridLayout
                className="layout"
                layout={layoutToRender}
                cols={columnsCount}
                rowHeight={30}
                width={1200}
                allowOverlap
                onDragStop={onLayoutChange}
                margin={[5,5]}
            >
                {layoutToRender.map(item => (
                    <div key={item.i} className="column-item">
                        <div className="column-content">{item.i}</div>
                    </div>
                ))}
            </GridLayout>
            <div>{renderScale}</div>
             {/* Для удобства тестирования на тачпаде */}
            <button onClick={() => handleWheel({preventDefault: ()=>{}, deltaY: 1})}>+</button><button onClick={() => handleWheel({preventDefault: ()=>{}, deltaY: -1})}>-</button>
         </div>
    );
}